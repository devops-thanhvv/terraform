variable "ecr_name" {
  description = "The list of ecr names to create"
  type        = list(string)
  default     = null
}

variable "tags" {
  description = "The key-value maps for tagging"
  type        = map(string)
  default     = {}
}

variable "image_mutability" {
  description = "Provide image mutability"
  type        = string
  default     = "MUTABLE"
}

variable "encrypt_type" {
  description = "Provide type of encryption here"
  type        = string
  default     = "KMS"
}

variable "aws_region" {
  default     = "ap-southeast-1"
  type        = string
  description = "Region in which AWS Resources to be created"
}
