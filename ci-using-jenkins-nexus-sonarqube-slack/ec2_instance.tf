resource "aws_instance" "nexus_server" {
  ami                         = var.ami_amz_linux_2
  instance_type               = var.instance_type_micro
  key_name                    = var.instance_keypair
  vpc_security_group_ids      = [aws_security_group.nexus_sg.id]
  subnet_id                   = var.subnet_id
  associate_public_ip_address = true
  user_data                   = filebase64("${path.module}/scripts/nexus_install.sh")
  tags                        = {
    Name = "nexus_server"
  }
}

# resource "aws_instance" "jenkins_server" {
#   ami                         = var.ami_amz_linux_2
#   instance_type               = var.instance_type_micro
#   key_name                    = var.instance_keypair
#   vpc_security_group_ids      = [aws_security_group.nexus_sg.id]
#   subnet_id                   = var.subnet_id
#   associate_public_ip_address = true
#   user_data                   = filebase64("${path.module}/scripts/nexus_install.sh")
#   tags                        = {
#     Name = "nexus_server"
#   }
# }