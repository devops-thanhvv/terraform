variable "aws_region" {
  default     = "ap-southeast-1"
  type        = string
  description = "Region in which AWS Resources to be created"
}

variable "instance_keypair" {
  default     = "terraform-key"
  type        = string
  description = "AWS EC2 Key pair that need to be associated with EC2 Instance"
}

variable "instance_type_micro" {
  default     = "t2.micro"
  type        = string
  description = "EC2 Instance Type - your computing, memory, networking, or storage needs."
}

variable "instance_type_medium" {
  default     = "t2.medium"
  type        = string
  description = "EC2 Instance Type - your computing, memory, networking, or storage needs."
}

variable "vpc_id" {
  default     = "vpc-030b1d10033146bc9"
  type        = string
  description = "The VPC that you want to launch your instance into."
}

variable "subnet_id" {
  default     = "subnet-04347966dfedc1db0"
  type        = string
  description = "The subnet in which the network interface is located."
}

variable "ami_ubuntu_20_04" {
  default     = "ami-002843b0a9e09324a"
  type        = string
  description = "Canonical, Ubuntu, 20.04 LTS, amd64 focal image build on 2023-05-17"
}

variable "ami_amz_linux_2" {
  default     = "ami-0d9efc67b4e551155"
  type        = string
  description = "Amazon Linux 2 Kernel 5.10 AMI 2.0.20230912.0 x86_64 HVM gp2"
}
