#### [You can find even more content at here](https://aws.plainenglish.io/devsecops-deploying-the-2048-game-on-docker-and-kubernetes-with-jenkins-ci-cd-3645a052c616)

#### Jenkins Plugin:
```
Eclipse Temurin Installer
Sonarqube Scanner
NodeJS
OWASP Dependency-Check
Docker
Docker Commons
Docker Pipeline
Docker API
docker-build-step
Kubernetes
Kubernetes Credentials
Kubernetes Client API
Kubernetes CLI
AWS Credentials
Amazon ECR
Gitlab
Git Parameter
```

#### Jenkins Credentials:
```
Add Secret Text     => sonar-token
Add AWS Credentials => aws-credentials
```

#### Jenkins Tool:
```
JDK installations                 => jdk17
SonarQube Scanner installations   => sonar-scanner
NodeJS installations              => node16
Dependency-Check installations    => DP-Check
```

#### Jenkins System:
```
SonarQube installations => sonar-server
```

#### Sonarqube Token:
```
Administration → Security → Users → Click on Tokens and Update Token → Give it a name → and click on Generate Token
```

#### Sonarqube Webhooks:
```
Administration → Configuration → Webhooks => URL: <http://jenkins-public-ip:8080>/sonarqube-webhook/
```

token-gitlab: glpat-QFpTMsuRsY9xhNCkKYY3
