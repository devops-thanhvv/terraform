resource "aws_instance" "devsecops_server" {
  ami                         = var.ami_ubuntu_22_04
  instance_type               = var.instance_type_large
  key_name                    = var.instance_keypair
  vpc_security_group_ids      = [aws_security_group.devsecops_group.id]
  subnet_id                   = var.subnet_id
  associate_public_ip_address = true
  user_data                   = filebase64("${path.module}/scripts/install.sh")
  tags = {
    Name = "devsecops_server"
  }
}
