output "instance_ip" {
  value      = aws_instance.devsecops_server.public_ip
  depends_on = [aws_instance.devsecops_server]
}
