resource "aws_instance" "prometheus_instance" {
  ami                         = var.ami
  instance_type               = var.instance_type
  key_name                    = var.instance_keypair
  vpc_security_group_ids      = [aws_security_group.metrics_group.id]
  associate_public_ip_address = true
  subnet_id                   = var.subnet_id
  user_data                   = filebase64("${path.module}/scripts/prometheus_install.sh")
  tags                        = {
    Name = "prometheus_instance"
  }
}

resource "aws_instance" "grafana_instance" {
  ami                         = var.ami
  instance_type               = var.instance_type
  vpc_security_group_ids      = [aws_security_group.metrics_group.id]
  associate_public_ip_address = true
  key_name                    = var.instance_keypair
  subnet_id                   = var.subnet_id
  user_data                   = filebase64("${path.module}/scripts/grafana_install.sh")
  tags                        = {
    Name = "grafana_instance"
  }
}
