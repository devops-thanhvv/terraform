variable "aws_region_1a" {
  default     = "ap-southeast-1a"
  type        = string
  description = "Region in which AWS Resources to be created"
}

variable "aws_region_1b" {
  default     = "ap-southeast-1b"
  type        = string
  description = "Region in which AWS Resources to be created"
}
