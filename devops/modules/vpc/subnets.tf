resource "aws_subnet" "public_subnet_1" {
  vpc_id                  = aws_vpc.devops_vpc.id
  cidr_block              = "10.11.1.0/24"
  availability_zone       = var.aws_region_1a
  map_public_ip_on_launch = true

  tags = {
    Name = "public-subnet-1"
  }
}

resource "aws_subnet" "public_subnet_2" {
  vpc_id                  = aws_vpc.devops_vpc.id
  cidr_block              = "10.11.2.0/24"
  availability_zone       = var.aws_region_1b
  map_public_ip_on_launch = true

  tags = {
    Name = "public-subnet-2"
  }
}
