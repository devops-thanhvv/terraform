resource "aws_internet_gateway" "devops_gw" {
  vpc_id = aws_vpc.devops_vpc.id
  tags = {
    Name = "devops_gw"
  }
}
