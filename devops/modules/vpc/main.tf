resource "aws_vpc" "devops_vpc" {
  cidr_block = "10.11.0.0/16"

  tags = {
    Name = "devops_vpc"
  }
}
