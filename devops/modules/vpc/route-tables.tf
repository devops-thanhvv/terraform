resource "aws_route_table" "devops_rtb" {
  vpc_id = aws_vpc.devops_vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.devops_gw.id
  }

  tags = {
    Name = "devops_rtb"
  }
}

resource "aws_route_table_association" "rtb_association_1" {
  subnet_id      = aws_subnet.public_subnet_1.id
  route_table_id = aws_route_table.devops_rtb.id
}

resource "aws_route_table_association" "rtb_association_2" {
  subnet_id      = aws_subnet.public_subnet_2.id
  route_table_id = aws_route_table.devops_rtb.id
}
