variable "instance_keypair" {
  default     = "terraform-key"
  type        = string
  description = "AWS EC2 Key pair that need to be associated with EC2 Instance"
}

variable "instance_type" {
  default     = "t2.micro"
  type        = string
  description = "EC2 Instance Type"
}

variable "ami_instance" {
  default     = "ami-0f98860b8bc09bd5c"
  type        = string
  description = "Amazon Linux 2023 AMI 2023.2.20231002.0 x86_64 HVM kernel-6.1"
}

variable "public_subnet" {
  default     = ""
  type        = string
  description = "Public subnet"
}

variable "vpc_id" {
  default     = ""
  type        = string
  description = "vpc"
}

variable "cidr_blocks" {
  default     = ["0.0.0.0/0"]
  type        = list(string)
  description = "cidr_blocks"
}
