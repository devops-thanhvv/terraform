resource "aws_instance" "gitlab_server" {
  ami                         = var.ami_instance
  instance_type               = var.instance_type
  associate_public_ip_address = true
  vpc_security_group_ids      = [aws_security_group.gitlab-sg.id]
  subnet_id                   = var.public_subnet
  key_name                    = var.instance_keypair
  user_data                   = filebase64("${path.module}/scripts/setup.sh")
  tags                        = {
    Name = "gitlab-server"
  }
}
