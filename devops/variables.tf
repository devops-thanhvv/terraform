variable "aws_region" {
  default     = "ap-southeast-1"
  type        = string
  description = "Region in which AWS Resources to be created"
}

variable "ami_instance" {
  default     = "ami-002843b0a9e09324a"
  type        = string
  description = "Canonical, Ubuntu, 20.04 LTS, amd64 focal image build on 2023-05-17"
}
