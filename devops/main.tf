module "vpc" {
  source = "./modules/vpc"
}

module "ec2" {
  depends_on = [module.vpc]

  source        = "./modules/ec2-gitlab"
  ami_instance  = var.ami_instance
  vpc_id        = module.vpc.vpc_id
  public_subnet = module.vpc.public_subnet_1
}
