### 1. Connect to server (name: kubectl-server-ec2)
### 2. Install aws v2 latest and configure
### 3. Setup kubectl
```sh
curl -O https://s3.us-west-2.amazonaws.com/amazon-eks/1.27.1/2023-04-19/bin/linux/amd64/kubectl
openssl sha1 -sha256 kubectl
chmod +x ./kubectl
mkdir -p $HOME/bin && cp ./kubectl $HOME/bin/kubectl && export PATH=$HOME/bin:$PATH
kubectl version --short --client
```
### 4. Set up our EKS Cluster on the Kubectl Server
```sh
aws eks --region ap-southeast-1 describe-cluster --name pc-eks --query cluster.status
aws eks --region ap-southeast-1 update-kubeconfig --name pc-eks
```
### 5. Check
```sh
kubectl get nodes -owide
```